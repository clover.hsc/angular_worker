# WebWorker

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.6.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Try web woker

1. Use `ng g web-worker app` to create default sample **ap.Worker.ts** and configure the **angular.json**, create **tsconfig.worker.json**, **app.worker.ts** update the **app.component.ts**.
  
2. Developers can create the `*.worker.ts` by themselves. All worker file names should be suffixed with .worker.ts

```typescript
// tsconfig.worker.json
{
  "extends": "./tsconfig.json",
  "compilerOptions": {
    "outDir": "./out-tsc/worker",
    "lib": [
      "es2018",
      "webworker"
    ],
    "types": []
  },
  "include": [
    "src/**/*.worker.ts"   // <-- *.worker.ts
  ]
}
```


# Ref

**[Improve Angular Page Performance ~ Web Worker](https://suneetbansal.medium.com/improve-angular-page-performance-web-worker-3c5edcd88b7c)**
