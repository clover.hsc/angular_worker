import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}

  bigLoop(): void {
    let j = 0;

    for (let i = 0; i <= 20000000000; i += 1) {
      j = i + 100 / 10;
    }
    alert(`Complete ${j} iterations`);
  }

  sayHello(): void {
    alert('Hello....');
  }

  worker(): void {
    const worker = new Worker('./bigLoop.worker', { type: 'module' });
    worker.onmessage = (event) => {
      alert(`Complete ${event.data} iterations`);
    };
  }
}
